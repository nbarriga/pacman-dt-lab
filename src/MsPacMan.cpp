/*
 * MsPacMan.cpp
 *
 *  Created on: Apr 24, 2018
 *      Author: nbarriga
 */

#include "MsPacMan.h"
#include <iostream>
MsPacMan::MsPacMan(int startNode):Character(startNode),score(0),lives(3) {
	// TODO Auto-generated constructor stub

}

MsPacMan::~MsPacMan() {
	// TODO Auto-generated destructor stub
}

void MsPacMan::die() {
	Character::die();
	lives--;
	std::cout<<lives<<" lives left!"<<std::endl;
	if(lives==0){
		exit(0);
	}
}

void MsPacMan::addScore(int points) {
	score+=points;
	std::cout<<"Score: "<<score<<std::endl;
}
bool MsPacMan::isDead() const {
	return lives == 0;
}

void MsPacMan::reset(int startNode)
{
	Character::reset(startNode);
	lives = 3;
}
