
#include "Maze.h"
#include "Game.h"
#include <iostream>


/*
 * If 'main' is defined we clear that definition
 * to get our default 'main' function back.
 */
#ifdef main
# undef main
#endif /* main */

int main(void) {


	Game g;
	g.run();

	return 0;
}
