/*
 * Game.cpp
 *
 *  Created on: Apr 17, 2018
 *      Author: VIDEOJUEGOS UTALCA
 */

#include "Game.h"
#include <algorithm>
#include "KeyboardController.h"
#include "RandomController.h"
#include "FSMController.h"
#include "BTGhostController.h"
#include "Ghost.h"
Game::Game():currentMap(0),
filenames{"mazes/a.txt","mazes/b.txt","mazes/c.txt","mazes/d.txt"},
gameState(filenames[currentMap]),
gv(std::make_unique<GameView>(std::vector<std::string>{"images/maze-a.png","images/maze-b.png","images/maze-c.png","images/maze-d.png"})) {

	auto pacman=std::make_shared<MsPacMan>(gameState.getMaze().getPacmanStart());
	gameState.addPacMan(pacman);
	pacmanControl=std::make_shared<KeyboardController>(pacman);

	std::vector<std::shared_ptr<Ghost>> ghosts;
	for(int i=0;i<4;i++){
		auto ghost=std::make_shared<Ghost>(gameState.getMaze().getGhostStart()[i]);
		ghosts.push_back(ghost);
	}
	gameState.addGhosts(ghosts);
	ghostsControl.push_back(std::make_shared<RandomController>(ghosts[0]));
	ghostsControl.push_back(std::make_shared<FSMController>(ghosts[1]));
	ghostsControl.push_back(std::make_shared<BTGhostController>(ghosts[2]));
	ghostsControl.push_back(std::make_shared<Controller>(ghosts[3]));
}

Game::~Game() {

}

void Game::run(){

	while(true){
		gv->draw(currentMap,gameState);
		gameState.updatePacman(pacmanControl->getMove(gameState));
		gameState.updateEaten();
		std::vector<Move> ghostMoves;
		std::transform(ghostsControl.begin(), ghostsControl.end(), std::back_inserter(ghostMoves), [this](const std::shared_ptr<Controller> &ghost) { return ghost->getMove(gameState);});
		gameState.updateGhosts(ghostMoves);
		gameState.updateEaten();
		if(gameState.won()){
			currentMap=(currentMap+1)%filenames.size();
			gameState.reset(filenames[currentMap]);
		}else if(gameState.lost()){
			break;
		}

	}


}


